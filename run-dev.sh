#!/bin/sh

docker build -f Dockerfile.dev -t decomposeme_convertnet:dev .
docker run --rm -ti -v $PWD:/code decomposeme_convertnet:dev 
docker rm -v $(docker ps -a -q -f status=exited)
docker rmi $(docker images -f "dangling=true" -q)

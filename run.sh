#!/bin/sh

docker build -f Dockerfile -t decomposeme_convertnet .
docker run --rm -ti -v $PWD:/code decomposeme_convertnet bash
docker rm -v $(docker ps -a -q -f status=exited)
docker rmi $(docker images -f "dangling=true" -q)

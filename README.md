DecomposeMe
===========

Converts traditional 2D convnets to decomposed 1D convnets (https://arxiv.org/abs/1606.05426)

```
./run-dev.sh
decomposeme_convertnet models/vgg-19.prototxt
```

Add the model `.prototxt` files you want to convert to the `models` folder.


License
-------

* Free software: MIT license

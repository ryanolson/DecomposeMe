#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import setup

#with open('README.rst') as readme_file:
#    readme = readme_file.read()

with open('HISTORY.rst') as history_file:
    history = history_file.read()

requirements = [
    'Click>=6.0',
    # TODO: put package requirements here
]

test_requirements = [
    # TODO: put package test requirements here
]

setup(
    name='decomposeme_convertnet',
    version='0.1.0',
    description="Converts traditional 2D convnets to decomposed 1D convnets (https://arxiv.org/abs/1606.05426)",
    # long_description=readme + '\n\n' + history,
    author="Ryan Olson",
    author_email='rolson@nvidia.com',
    url='https://github.com/ryanolson/decomposeme_convertnet',
    packages=[
        'decomposeme_convertnet',
    ],
    package_dir={'decomposeme_convertnet':
                 'decomposeme_convertnet'},
    entry_points={
        'console_scripts': [
            'decomposeme_convertnet=decomposeme_convertnet.cli:main'
        ]
    },
    include_package_data=True,
    install_requires=requirements,
    license="MIT license",
    zip_safe=False,
    keywords='decomposeme_convertnet',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Natural Language :: English',
        "Programming Language :: Python :: 2",
        'Programming Language :: Python :: 2.6',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
    ],
    test_suite='tests',
    tests_require=test_requirements
)

#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
test_decomposeme_convertnet
----------------------------------

Tests for `decomposeme_convertnet` module.
"""

import pytest

from contextlib import contextmanager
from click.testing import CliRunner

from decomposeme_convertnet import DecomposeMe
from decomposeme_convertnet import cli


@pytest.fixture
def lenet():
    return DecomposeMe("models/lenet-digits.prototxt")

@pytest.fixture
def vgg19():
    return DecomposeMe("models/vgg-19.prototxt")


class TestDecomposeLeNet(object):
    def test(self, lenet):
        conv2d = lenet.conv_2d_layers
        assert len(conv2d) == 2
        lenet.convert()
        assert len(lenet.conv_layers) == 2


class TestDecomposeMeVGG19(object):

    def test_2d_layers(self, vgg19):
        assert len(vgg19.conv_layers) == 16
        vgg19.convert()
        assert len(vgg19.conv_layers) == 32
        assert len(vgg19.conv_2d_layers) == 0

FROM nvidia/caffe

RUN apt-get update && apt-get install -y git vim python-dev python-pip protobuf-compiler libffi-dev libssl-dev \
 && pip install --upgrade pip

COPY requirements_dev.txt /tmp
RUN pip install -r /tmp/requirements_dev.txt

COPY * /code/
WORKDIR /code
RUN python setup.py develop

VOLUME /models
WORDIR /models
CMD ["bash"]

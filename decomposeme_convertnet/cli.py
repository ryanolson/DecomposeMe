# -*- coding: utf-8 -*-
import click
from decomposeme_convertnet import DecomposeMe

@click.command()
@click.argument('prototxt', type=click.File('r'))
def main(prototxt):
    """Console script for decomposeme_convertnet"""
    click.echo(prototxt.name)

    dcme = DecomposeMe(prototxt.name)
    dcme.convert()
    print str(dcme.net)


if __name__ == "__main__":
    main()

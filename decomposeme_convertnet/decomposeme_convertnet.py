# -*- coding: utf-8 -*-
import code
import itertools
import os
import shutil
import subprocess
import tempfile

from caffe.proto import caffe_pb2
from google.protobuf.text_format import Merge, MessageToString


class DecomposeMe(object):

    def __init__(self, prototxt, include=None, exclude=None):
        self._input = self.import_prototxt(prototxt)
        self._output = caffe_pb2.NetParameter()
        self._output.CopyFrom(self._input)

    def import_prototxt(self, prototxt):
        try:
            temp_dir = tempfile.mkdtemp()
            temp_file = os.path.join(temp_dir, "converted.prototxt")
            subprocess.call([
                "caffe_upgrade_net_proto_text", prototxt, temp_file
            ])
            net = caffe_pb2.NetParameter()
            with open(temp_file, "r") as file:
                Merge(str(file.read()), net)
            return net
        finally:
            shutil.rmtree(temp_dir)

    @property
    def net(self):
        return self._output

    @property
    def layers(self):
        return self.net.layer

    @staticmethod
    def is_conv_layer(layer):
        """
        Returns True if the layer is eligible for decomposition.
        """
        if isinstance(layer.type, int):
            return layer.type == caffe_pb2.V1LayerParameter.CONVOLUTION
        elif isinstance(layer.type, basestring):
            return layer.type.lower() == "convolution"
        raise ValueError("unknown layer type")

    @staticmethod
    def is_activation_layer(layer):
        return layer.type.lower() in ["sigmoid", "tanh", "relu"]

    def is_2d_conv_layer(self, layer):

        if self.is_conv_layer(layer):
            params = layer.convolution_param

            # kernel_size
            if params.kernel_h or params.kernel_w:
                temp = [params.kernel_h, params.kernel_w]
                params.ClearField("kernel_h")
                params.ClearField("kernel_w")
                params.kernel_size.extend(temp)
            if len(params.kernel_size) == 1:
                params.kernel_size.append(params.kernel_size[0])

            # stride
            if params.stride_h or params.stride_w:
                temp = [params.stride_h, params.stride_w]
                params.ClearField("stride_h")
                params.ClearField("stride_w")
                params.stride.extend(temp)
            if not params.stride:
                params.stride.extend([1,1])
            if len(params.stride) == 1:
                params.stride.append(params.stride[0])

            # padding
            if params.pad_h or params.pad_w:
                temp = [pad_h, pad_w]
                params.ClearField("pad_h")
                params.ClearField("pad_w")
                params.pad.extend(temp)
            if not params.pad:
                params.pad.extend([0,0])
            if len(params.pad) == 1:
                params.pad.append(params.pad[0])

            if params.kernel_size[0] > 1 and params.kernel_size[1] > 1:
                return True

    @property
    def conv_layers(self):
        """
        Returns a list of eligible convolution layers for decomposing.
        """
        return [l  for l in self.layers if self.is_conv_layer(l)]

    @property
    def conv_2d_layers(self):
        """
        Returns a list of eligible 2D convolution layers for decomposing.
        """
        return [l for l in self.conv_layers if self.is_2d_conv_layer(l)]

    def find_layer(self, layer_name):
        for idx, layer in enumerate(self.layers):
            if layer.name == layer_name:
                return (idx, layer)
        raise ValueError("{0} layer not found.".format(layer_name))

    def decompose_layer(self, layer, depth=None, transposed=False,
                        weight_filler=None, bias_filler=None):
        """
        Decompose a 2D convolution layer into 2x 1D layers.
        """
        if not self.is_2d_conv_layer(layer):
            raise ValueError("not a 2d layer")
        activation = self.linked_activation(layer)
        if not activation:
            raise ValueError("no activation layer associated with {0}".format(layer.name))

        # find activation function
        # throw error if no dependent activation is found

        weight_filler = weight_filler or caffe_pb2.FillerParameter(type="xavier", std=0.1)
        bias_filler = bias_filler or caffe_pb2.FillerParameter(type="constant", value=0.2)
        layer.convolution_param.weight_filler.MergeFrom(weight_filler)
        layer.convolution_param.bias_filler.MergeFrom(bias_filler)

        depth = depth or layer.convolution_param.num_output
        kernel = layer.convolution_param.kernel_size
        stride = layer.convolution_param.stride
        pad = layer.convolution_param.pad
        name = ["-dcme-1", "-dcme-2"]
        kernels = [ [kernel[0], 1], [1, kernel[1]] ]
        strides = [ [stride[0], 1], [1, stride[1]] ]
        pads = [ [pad[0], 0], [0, pad[1]] ]
        if transposed:
            kernels.reverse()
            strides.reverse()
            pads.reverse()

        l1 = caffe_pb2.LayerParameter()
        l2 = caffe_pb2.LayerParameter()
        act = caffe_pb2.LayerParameter()

        for l in [l1, l2]:
            l.MergeFrom(layer)
            l.ClearField("top")
            l.convolution_param.ClearField("kernel_size")
            l.convolution_param.ClearField("stride")
            l.convolution_param.ClearField("pad")

        layers = itertools.izip([l1,l2], name, kernels, strides, pads)
        for (l, name, kernel, stride, pad) in layers:
            #print kernel, stride, pad
            l.name = l.name + name
            l.top.append(l.name)
            l.convolution_param.kernel_size.extend(kernel)
            l.convolution_param.stride.extend(stride)
            l.convolution_param.pad.extend(pad)


        del l2.bottom[:]
        l2.bottom.append(l1.name)

        act.name = l1.name + "-" + activation.type
        act.type = activation.type
        act.bottom.append(l1.name)
        act.top.append(l1.name)

        bottoms = [l for l in self.layers if layer.name in l.bottom]
        tops = [l for l in self.layers if layer.name in l.top]

        # reset dependents to the last l2
        for l in bottoms:
            l.bottom.remove(layer.name)
            l.bottom.append(l2.name)
        for l in tops:
            l.top.remove(layer.name)
            l.top.append(l2.name)


        index, _ = self.find_layer(layer.name)
        mutable_layers = [l for l in self.layers]
        del mutable_layers[index]
        for l in [l2, act, l1]:
            mutable_layers.insert(index, l)

        del self.layers[:]
        self.layers.extend(mutable_layers)

    def linked_activation(self, layer):
        dependents = [l for l in self.layers if layer.name in l.bottom]
        activations = [l for l in dependents if self.is_activation_layer(l)]
        if len(activations) > 1:
            raise ValueError("unexpected number of activations")
        return activations.pop() if activations else None

    def convert(self):
        for layer in self.conv_2d_layers:
            if self.linked_activation(layer):
                #print "decomposing ", layer.name
                self.decompose_layer(layer)
            else:
                pass
                #print "no activation following ", layer.name
